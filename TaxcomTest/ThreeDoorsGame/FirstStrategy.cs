﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaxcomTest.ThreeDoorsGame
{
    public class FirstStrategy : IStrategy
    {
        public string Description  => "Игрок не изменяет первоначальный выбор"; 

        public InDoorContentType Do(Door selected, Door[] doors)
        {
            return selected.Open();
        }
    }
}