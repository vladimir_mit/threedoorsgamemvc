﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaxcomTest.ThreeDoorsGame
{
    public class SecondStrategy : IStrategy
    {
        public string Description => "Игрок меняет первоначальный выбор";

        public InDoorContentType Do(Door selected, Door[] doors)
        {
            //если первоначальный выбор не верный - игрок получает приз
            return (selected.Open() == InDoorContentType.Rubbish) ? InDoorContentType.Prize : InDoorContentType.Rubbish;
        }
    }
}