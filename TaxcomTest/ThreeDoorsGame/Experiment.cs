﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace TaxcomTest.ThreeDoorsGame
{
    public class Experiment
    {
        private GameCreator _gameCreator = new GameCreator();

        public Dictionary<string, List<GameStatus>> Process(int doorsNumber = 3, int times = 1000, params IStrategy[] strategies)
        {
            if (strategies == null || strategies.Count() == 0)
            {
                strategies = Assembly.GetAssembly(typeof(IStrategy)).GetTypes()
                    .Where(x => x.GetInterfaces().Contains(typeof(IStrategy)))
                    .Select(x => (IStrategy)Activator.CreateInstance(x))
                    .ToArray();
            }
               
            var result = new Dictionary<string, List<GameStatus>>();

            GameCreatorConfiguration config = new GameCreatorConfiguration();
            config.DoorsNumber = doorsNumber;

            _gameCreator.Config = config;

            foreach (var s in strategies)
            {
                config.Strategy = s;
                var game = _gameCreator.Create();
                var resultsList = new List<GameStatus>();

                for (int i = 0; i < times; i++)
                {
                   resultsList.Add(game.Play());
                }

                result.Add(s.Description, resultsList);
            }

            return result;
        }
    }
}