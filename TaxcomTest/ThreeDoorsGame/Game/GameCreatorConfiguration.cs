﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaxcomTest.ThreeDoorsGame
{
    public class GameCreatorConfiguration
    {
        public IStrategy Strategy { get; set; } = new FirstStrategy(); //DefaultConfig
        public int DoorsNumber { get; set; } = 3; //DefaultConfig
    }
}