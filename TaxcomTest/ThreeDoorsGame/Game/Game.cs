﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaxcomTest.ThreeDoorsGame
{
    public class Game
    {
        private Door[] _doors;
        private IStrategy _strategy;
        private Random _rnd;

        public Game(IStrategy strategy, Door[] doors, Random rnd)
        {
            _strategy = strategy;
            _rnd = rnd;
            _doors = doors;
        }

        public GameStatus Play()
        {
            var selectedDoor = _doors[_rnd.Next(0, _doors.Length)];
            var prize = _strategy.Do(selectedDoor, _doors);

            var result = (prize == InDoorContentType.Prize) ? GameStatus.Win : GameStatus.Loss;

            return result;
        }

        public int ChooseDoor(int doorNumber) //returns number of suggested door
        {
            var selectedDoor = _doors[doorNumber];

            var suggetion = 
                (selectedDoor.Open() != InDoorContentType.Prize)
                ?
                    _doors.FirstOrDefault(x => x != selectedDoor && x.Open() == InDoorContentType.Prize)
                :
                    _doors.Where(x => x != selectedDoor).ToArray()[_rnd.Next(0, _doors.Length - 1)];

            return Array.IndexOf(_doors, suggetion);
        }

        public GameStatus OpenDoor(int doorNumber)
        {
            var prize = _doors[doorNumber].Open();
            var result = (prize == InDoorContentType.Prize) ? GameStatus.Win : GameStatus.Loss;

            return result;
        }
    }
}