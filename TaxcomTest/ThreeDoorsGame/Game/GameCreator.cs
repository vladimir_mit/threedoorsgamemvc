﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaxcomTest.ThreeDoorsGame
{
    public class GameCreator
    {
        public GameCreatorConfiguration Config { get; set; } = new GameCreatorConfiguration();
        private Random _rnd = new Random();

        public Game Create()
        {
            var doors = new Door[Config.DoorsNumber];
            var luckyNumber = _rnd.Next(0, doors.Length);

            for (int i = 0; i < doors.Length; i++)
            {
                doors[i] = (i == luckyNumber) ? new Door(InDoorContentType.Prize) : new Door(InDoorContentType.Rubbish);
            }

            var game = new Game(Config.Strategy, doors, _rnd);
            return game;
        }
    }
}