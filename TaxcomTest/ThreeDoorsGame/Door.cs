﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaxcomTest.ThreeDoorsGame
{
    public class Door
    {
        private InDoorContentType _content;

        public Door(InDoorContentType content)
        {
            _content = content;
        }

        public InDoorContentType Open()
        {
            return _content;
        }
    }
}