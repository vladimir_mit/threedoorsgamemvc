﻿namespace TaxcomTest.ThreeDoorsGame
{
    public interface IStrategy
    {
        InDoorContentType Do(Door selected, Door[] doors);
        string Description { get; }
    }
}