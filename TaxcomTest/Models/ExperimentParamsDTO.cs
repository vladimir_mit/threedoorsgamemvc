﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaxcomTest.Models
{
    public class ExperimentParamsDTO
    {
        public int? DoorsNumber { get; set; }

        public int? Times { get; set; }
    }
}