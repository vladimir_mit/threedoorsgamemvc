﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaxcomTest.Models;
using TaxcomTest.ThreeDoorsGame;

namespace TaxcomTest.Controllers
{
    public class HomeController : Controller
    {

        [HttpGet]
        public ActionResult Index(ExperimentParamsDTO dto)
        {
            var exp = new Experiment();

            var result = (dto.DoorsNumber == null && dto.Times == null) ? exp.Process() : exp.Process(doorsNumber: (int)dto.DoorsNumber, times: (int)dto.Times);

            var stat = result.ToDictionary(x => x.Key, x => x.Value.Where(y => y == GameStatus.Win).Count() * 1.0 / x.Value.Count);
            ViewBag.Stat = stat;

            return View();
        }

    }
}